# Bengali Dictionary
#### A Large Collection of Bengali Words & Translations

### Description

BengaliWordList\_48.txt, BengaliWordList\_112.txt, & BengaliWordList\_439.txt contains more than 48000, 112000, & 439000 Bengali words respectively. These lists can be used for spell checking and many more other purposes. The file is encoded in UTF-8 & words are separated with new line. Shorter list contains words only of higher frequancy.

BengaliDictionary\_17.rar, BengaliDictionary\_36.rar, & BengaliDictionary\_93.rar contains 17000, 36000, & 93000 English to Bengali translations. Files are encoded in UTF-8, and each translation is seperated with new line.

<div align="center">
  <img src="https://cloud.githubusercontent.com/assets/5456665/19994067/c1d80750-a274-11e6-8160-2af151f5e966.png" height="400" width=auto title="bengali word list preview" />
  <img src="https://cloud.githubusercontent.com/assets/5456665/20518189/6de784c4-b0c8-11e6-900b-34d5338088be.png" height="400" width=auto title="bengali word translation preview" />
</div>
 
### Download
- Word List
  - [BengaliWordList\_40](https://github.com/MinhasKamal/BengaliDictionary/raw/download/BengaliWordList_40.rar)
  - [BengaliWordList\_48](https://github.com/MinhasKamal/BengaliDictionary/raw/download/BengaliWordList_48.rar)
  - [BengaliWordList\_112](https://github.com/MinhasKamal/BengaliDictionary/raw/download/BengaliWordList_112.rar)
  - [BengaliWordList\_439](https://github.com/MinhasKamal/BengaliDictionary/raw/download/BengaliWordList_439.rar)
- Word Translation
  - [BengaliDictionary\_17.rar](https://github.com/MinhasKamal/BengaliDictionary/raw/download/BengaliDictionary_17.rar)
  - [BengaliDictionary\_36.rar](https://github.com/MinhasKamal/BengaliDictionary/raw/download/BengaliDictionary_36.rar)
  - [BengaliDictionary\_93.rar](https://github.com/MinhasKamal/BengaliDictionary/raw/download/BengaliDictionary_93.rar)
- Other Resources
  - [BengaliCharacterCombinations](https://github.com/MinhasKamal/BengaliDictionary/raw/download/BengaliCharacterCombinations.rar)
  - [BengaliPhoneticWordFrequency\_164](https://github.com/MinhasKamal/BengaliDictionary/raw/download/BengaliPhoneticWordFrequency_164.rar)

### License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-88x31.png" /></a><br/>BengaliDictionary is licensed under a <a rel="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License version-3</a>.
